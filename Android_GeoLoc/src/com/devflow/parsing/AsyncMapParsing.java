package com.devflow.parsing;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.app.ProgressDialog;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.devflow.tsunami.FilmData;
import com.devflow.tsunami.HelloItemizedOverlay;
import com.devflow.tsunami.R;
import com.devflow.util.Util;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapController;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;

//Params, Progress, Result
public class AsyncMapParsing extends AsyncTask<String, Void, Boolean> {

    private ProgressDialog dialog;
    private Context context = null;
    private ListView resultListView = null;
    private ArrayList<String> currentLocList = null;
    private FilmData filmData = null;
    private Geocoder geoCoder = null;
    private MapController myMapController;
    private ArrayList<Address> locationList = new ArrayList<Address>();
    private List<Overlay> mapOverlays;
    private HelloItemizedOverlay<OverlayItem> itemizedoverlay;

    /**
     * Sets all necessary references
     */
    public AsyncMapParsing(Context context, List<Overlay> mapOverlays,
            HelloItemizedOverlay<OverlayItem> itemizedoverlay, MapController myMapController, FilmData filmData)
    {
        this.context = context;
        this.mapOverlays = mapOverlays;
        this.itemizedoverlay = itemizedoverlay;
        this.myMapController = myMapController;
        this.filmData = filmData;
        this.dialog = new ProgressDialog(context);
        geoCoder = new Geocoder(context);
    }

    /**
     * Show the loading dialog before doing the work
     */
    @Override
    protected void onPreExecute()
    {
        this.dialog.setMessage("Loading...");
        this.dialog.show();
    }

    /**
     * Get the location by calling the location parser
     */
    @Override
    protected Boolean doInBackground(String... params)
    {
        String url = params[0];
        LocationParser locPar = new LocationParser();
        try {
            currentLocList = locPar.parseLocationsHTML(url);
            return true;
        }
        catch (Exception e) {
            e.printStackTrace();
            return false;
        }


    }

    /**
     * Dismiss the dialog after the work is done. Also, display results to the
     * ListView
     */
    @Override
    protected void onPostExecute(final Boolean success)
    {
        // Depending on when the operation was successful
        if (success) {
            addToMapView();
        }
        else {
            Toast.makeText(context, "Error loading data, check Internet connection",
                    Toast.LENGTH_LONG).show();
            GeoPoint point = new Util.LatLonPoint(Util.lat,Util.lng);
            OverlayItem overlayitem = new OverlayItem(point, "", "");
            itemizedoverlay.addOverlay(overlayitem);
            mapOverlays.add(itemizedoverlay);
        }

        if(dialog.isShowing()) dialog.dismiss();
    }

    /**
     * Takes care of displaying results
     */
    public void displayToResultList()
    {
        filmData.setFilmLocations(this.currentLocList);

        Log.d("ResultingLocationInAsync", this.currentLocList.toString());
        if (this.currentLocList == null || this.currentLocList.isEmpty()) {
            this.currentLocList = new ArrayList<String>();
            this.currentLocList.add("No other results found");
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, R.layout.location_row,
                R.id.label, this.currentLocList);
        resultListView.setAdapter(adapter);
    }

    /**
     * Add results to the to the locationList (ArrayList<Address>) of the
     * current film data
     */
    private void addToMapView()
    {
        if (currentLocList.size() > 0) {

            try {
                for (String location : currentLocList) {
                    List<Address> result = geoCoder.getFromLocationName(location, 3);

                    if (result != null && result.size() > 0) {
                        locationList.add(result.get(0));
                        Address address = result.get(0);

                        GeoPoint point = new LatLonPoint(address.getLatitude(),
                                address.getLongitude());
                        OverlayItem overlayitem = new OverlayItem(point, "Hi", "There");
                        itemizedoverlay.addOverlay(overlayitem);
                    }
                    else {
                        System.out.println("Can't get address");
                    }

                }

            }
            catch (IOException e) {
                e.printStackTrace();
            }

        }
        else {
            System.out.println("No results, so simply plot the current location");
            GeoPoint point = new Util.LatLonPoint(Util.lat,Util.lng);
            OverlayItem overlayitem = new OverlayItem(point, "", "");
            itemizedoverlay.addOverlay(overlayitem);
        }

        mapOverlays.add(itemizedoverlay);
        myMapController.animateTo(new Util.LatLonPoint(Util.lat, Util.lng));
    }

    private static final class LatLonPoint extends GeoPoint {
        public LatLonPoint(double latitude, double longitude)
        {
            super((int) (latitude * 1E6), (int) (longitude * 1E6));
        }
    }


}
