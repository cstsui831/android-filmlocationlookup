package com.devflow.parsing;

import java.io.IOException;
import java.util.ArrayList;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.ListView;
import android.widget.Toast;

import com.devflow.tsunami.FilmData;
import com.devflow.tsunami.FilmDataAdapter;
import com.devflow.tsunami.FilmDataEndlessAdapter;
import com.devflow.tsunami.R;

///Params, Progress, Result
public class AsyncResultParsing extends AsyncTask<String, Void, Boolean> {

    private ArrayList<FilmData> currentFilmList;
    private Context context;
    private ListView resultListView;
    private ResultParser resultParser = null;
    private FilmDataAdapter adapter;

    /**
     * Sets all necessary references
     */
    public AsyncResultParsing(Context context, ResultParser resultParser, ArrayList<FilmData> currentFilmList,
            ListView resultList, FilmDataAdapter adapter)
    {
        this.context = context;
        this.currentFilmList = currentFilmList;
        this.resultListView = resultList;
        this.resultParser = resultParser;
        this.adapter = adapter;
    }


    /**
     * Pre execute work
     */
    @Override
    protected void onPreExecute()
    {
    }


    /**
     * Do background work
     */
    @Override
    protected Boolean doInBackground(String... params)
    {
        String url = params[0];
        ArrayList<FilmData> filmList = null;
        try {
            filmList = resultParser.parseHTML(url);
        }
        catch (IOException e) {
            e.printStackTrace();
            return false;
        }


        //Clear, because if address changed we want to get rid of the old data
        this.currentFilmList.clear();
        for (FilmData title : filmList) {
            this.currentFilmList.add(title);
        }
        System.out.println(this.currentFilmList.toString());
        return true;
    }


    /**
     * After executing work
     */
    @Override
    protected void onPostExecute(final Boolean success)
    {
        if (success) {
            displayToResultList();
            addEndlessAdapter();
            resultParser.setFirstListDone(true);
        }
        else {
            Toast.makeText(context, "Error loading data, check Internet connection", Toast.LENGTH_LONG).show();
        }

    }


    /**
     * When this async task completes, I now wrap it in an endless wrapper to update the list when scrolled to the bottom
     */
    private void addEndlessAdapter()
    {
        FilmDataEndlessAdapter endlessAdapter = new FilmDataEndlessAdapter(context, adapter, R.layout.loading_layout);
        resultListView.setAdapter(endlessAdapter);

    }


    /**
     * Sets lists
     */
    private void displayToResultList()
    {
        if (currentFilmList != null) {
            adapter.notifyDataSetChanged();
        }
    }
}
