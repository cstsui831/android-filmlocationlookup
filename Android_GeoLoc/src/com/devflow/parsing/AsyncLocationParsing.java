package com.devflow.parsing;

import java.io.IOException;
import java.util.ArrayList;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.devflow.tsunami.FilmData;
import com.devflow.tsunami.R;

//Params, Progress, Result
public class AsyncLocationParsing extends AsyncTask<String, Void, Boolean> {

    private Context context = null;
    private ListView resultListView = null;
    private ArrayList<String> currentLocList = null;
    private FilmData filmData = null;

    /**
     * Sets all necessary references
     */
    public AsyncLocationParsing(Context context, ListView resultListView, FilmData filmData)
    {
        this.context = context;
        this.resultListView = resultListView;
        this.filmData = filmData;
    }

    /**
     * Show the loading dialog before doing the work
     */
    @Override
    protected void onPreExecute()
    {
    }

    /**
     * Get the location by calling the location parser
     */
    @Override
    protected Boolean doInBackground(String... params)
    {
        String url = params[0];
        try {
            LocationParser locPar = new LocationParser();
            currentLocList = locPar.parseLocationsHTML(url);
            return true;
        }

        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Dismiss the dialog after the work is done. Also, display results to the
     * ListView
     */
    @Override
    protected void onPostExecute(final Boolean success)
    {
        // Depending on when the operation was successful
        if (success) {
            displayToResultList();

        }
        else {
            Toast.makeText(context, "Error loading data, check Internet connection",
                    Toast.LENGTH_LONG).show();
            new AsyncLocationParsing(context, resultListView, filmData).execute(filmData.getImdbLink());
        }

    }

    /**
     * Takes care of displaying results
     */
    public void displayToResultList()
    {
        filmData.setFilmLocations(this.currentLocList);

        Log.d("ResultingLocationInAsync", this.currentLocList.toString());
        if (this.currentLocList == null || this.currentLocList.isEmpty()) {
            this.currentLocList = new ArrayList<String>();
            this.currentLocList.add("No other results found");
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, R.layout.location_row,
                R.id.label, this.currentLocList);
        resultListView.setAdapter(adapter);
    }


}
