package com.devflow.parsing;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import android.util.Log;

import com.devflow.tsunami.FilmData;
import com.devflow.util.Util;

//Use link.attr("abs:href") to get the actual link
//Use link.text() to get the "name" of the link

public class ResultParser {

    // ------------------------------------------------------------------------
    // Fields
    // ------------------------------------------------------------------------
    private boolean hasPrev = false;
    private boolean hasNext = false;

    private boolean firstListDone = false;

    private String prevPageLink = "";
    private String nextPageLink = "";

    private LocationParser locParser = null;

    // ------------------------------------------------------------------------
    // Constructor
    // ------------------------------------------------------------------------
    public ResultParser()
    {

        this.locParser = new LocationParser();
    }

    // ------------------------------------------------------------------------
    // Methods
    // ------------------------------------------------------------------------

    /**
     * Parse the search research data got from IMDb For reference: Elements
     * links = doc.select("a[href]"); <- get links
     */
    public ArrayList<FilmData> parseHTML(String url) throws IOException
    {

        outText("URL: " + url + "\n");
        Document doc = null;
        Element table = null;
        ArrayList<FilmData> filmList = new ArrayList<FilmData>();

        // ** First Level Parsing: get the table of results **//
        // Try to get the document from the url; after that, get the results
        try {
            doc = Jsoup.connect(url).userAgent(Util.userAgent).get();
            table = doc.select("table[class=results]").first();
        }
        catch (Exception e) {
            Log.e(Util.Tag, e.toString());
        }

        // ** Second level parsing: get each title **//
        // If table isn't null, that means the results were found
        if (table != null) {
            Iterator<Element> tableElement = table.select("td[class=title]").iterator();
            while (tableElement.hasNext()) {
                Element info = tableElement.next();

                // ** Third level parsing: get each link of each title **//
                Element link = info.select("a[href]").first();
                Element year = info.select("span[class=year_type]").first();

                // Create a new film object and add it to the list
                FilmData film = new FilmData();
                film.setName(link.text());
                film.setYear(year.text());
                film.setImdbLink(link.attr("abs:href"));
                filmList.add(film);
            }
        }
        else {
            Log.d(Util.Tag, "No results found");
            outText("No results found");
        }

        // Set all the Film class information here
        for (FilmData film : filmList) {
            Log.d(Util.Tag, film.toString());
            outText("*" + film.toString());
            outText("**" + film.getImdbLink());
            outText("\n");
        }

        // Next stage, check for more results
        // checkNext(doc);
        checkPrevOrNext(doc);
        return filmList;
    }

    /**
     * Check availability of "previous" or "next" page links
     */
    public void checkPrevOrNext(Document doc)
    {
        // Check for next page here
        if (doc == null)
            return;

        // Find next or prev here
        Element leftright = doc.select("div[class=leftright]").get(1);

        // Get 1 means get the second one, the first one being the top of
        // the page
        Log.d(Util.Tag, leftright.text());
        Elements prevNextLinks = leftright.select("a[href]");

        // If there are two links, that means both prev and next are there
        if (prevNextLinks.size() == 2) {
            this.hasPrev = true;
            this.hasNext = true;
            prevPageLink = prevNextLinks.get(0).attr("abs:href");
            nextPageLink = prevNextLinks.get(1).attr("abs:href");
        }
        // Only contains one link
        else if (prevNextLinks.size() == 1) {

            // Check for previous page link
            if (prevNextLinks.get(0).text().contains("Prev")) {
                this.hasPrev = true;
                this.hasNext = false;
                prevPageLink = prevNextLinks.get(0).attr("abs:href");
                Log.d(Util.Tag, prevNextLinks.get(0).text());
                // outText(prevNextLinks.get(0).text());
            }
            // Check for next page link
            else {
                this.hasNext = true;
                this.hasPrev = false;
                nextPageLink = prevNextLinks.get(0).attr("abs:href");
                Log.d(Util.Tag, prevNextLinks.get(0).text());
                // outText(prevNextLinks.get(0).text());
            }
        }

        else {
            this.hasPrev = false;
            this.hasNext = false;
            // if no links, that means no results are found
        }

        // Log.d(Util.Tag,"Has Prev: " + hasPrev + "\n" + "Has Next: " +
        // hasNext);
        // outText(prevNextLinks.get(0).text());
    }

    public LocationParser getLocParser()
    {
        return locParser;
    }

    public void setLocParser(LocationParser locParser)
    {
        this.locParser = locParser;
    }

    /**
     * Output text on the big bottom textview
     */
    private void outText(String line)
    {
        // outputText.append(line + "\n");
    }

    // ------------------------------------------------------------------------
    // Getters and Setters
    // ------------------------------------------------------------------------
    public String getPrevPageLink()
    {
        return prevPageLink;
    }

    public void setPrevPageLink(String prevPageLink)
    {
        this.prevPageLink = prevPageLink;
    }

    public String getNextPageLink()
    {
        return nextPageLink;
    }

    public void setNextPageLink(String nextPageLink)
    {
        this.nextPageLink = nextPageLink;
    }

    public boolean hasPrev()
    {
        return this.hasPrev;
    }

    public boolean hasNext()
    {
        return this.hasNext;
    }

    public boolean isFirstListDone()
    {
        return firstListDone;
    }

    public void setFirstListDone(boolean firstListDone)
    {
        this.firstListDone = firstListDone;
    }
}
