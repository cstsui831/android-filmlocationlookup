package com.devflow.parsing;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import android.util.Log;

import com.devflow.util.Util;

public class LocationParser {

    // ------------------------------------------------------------------------
    // Constructor
    // ------------------------------------------------------------------------
    public LocationParser(){
    }


    // ------------------------------------------------------------------------
    // Methods
    // ------------------------------------------------------------------------
    //URL argment passed in is the film title information url
    public ArrayList<String> parseLocationsHTML(String url) throws Exception {
        ArrayList<String> locationList = new ArrayList<String>();
        url = url + "locations";
        Log.d(Util.Tag,"URL: " + url);
        Document doc = null;
        Element locations = null;

        // ** First Level Parsing: get the table of results **//
        // Try to get the document from the url; after that, get the results
        try {
            doc = Jsoup.connect(url).userAgent(Util.userAgent).get();
            locations = doc.select("dl").first();
        } catch (Exception e) {
            Log.e(Util.Tag,e.toString());
        }

        //TODO need to test against page with no results
        if (locations != null) {
            Iterator<Element> locationLinks = locations.select("a[href]").iterator();
            while (locationLinks.hasNext()) {
                String locationString = locationLinks.next().text();
                locationList.add(locationString);
            }
            Log.d(Util.Tag,locationList.toString());
            //outputText.append("\n" + locationList.toString());

        }else {
            Log.d(Util.Tag,"No locations found");
            //outputText.append("No locations found");
        }
        return locationList;
    }
}
