package com.devflow.util;

import com.google.android.maps.GeoPoint;

public class Util {
    public static final String userAgent =
    		//"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21";
              "Mozilla/6.0 (Macintosh; I; Intel Mac OS X 11_7_9; de-LI; rv:1.9b4) Gecko/2012010317 Firefox/10.0a4";
    public static final String locationURL =
    		"http://www.imdb.com/search/text?realm=title&field=locations&q=hong+kong";

    public static final String Tag = "Parse";

    public static final String STREET  = "STREET";
    public static final String CITY    = "CITY";
    public static final String STATE   = "STATE";
    public static final String COUNTRY = "COUNTRY";

    public static final String FILMLOCATION = "FILMLOCATION";
    public static final String ACTORORIGIN  = "ACTORORIGIN";

    public static double lat = 0;
    public static double lng = 0;


    public static final class LatLonPoint extends GeoPoint {
        public LatLonPoint(double latitude, double longitude)
        {
            super((int) (latitude * 1E6), (int) (longitude * 1E6));
        }
    }
}
