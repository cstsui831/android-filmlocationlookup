package com.devflow.tsunami;

import java.io.Serializable;
import java.util.ArrayList;

import android.location.Address;

@SuppressWarnings("serial")
public class FilmData implements Cloneable, Serializable{

    // ------------------------------------------------------------------------
    // Fields
    // ------------------------------------------------------------------------
    private String            name          = "";
    private String            yearType      = "";
    private String            imdbLink      = "";
    private ArrayList<String> filmLocations = new ArrayList<String>();
    private ArrayList<Address> locationList = new ArrayList<Address>();
    // ------------------------------------------------------------------------
    // Setters and Getters
    // ------------------------------------------------------------------------
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public ArrayList<Address> getLocationList()
    {
        return locationList;
    }
    public void setLocationList(ArrayList<Address> locationList)
    {
        this.locationList = locationList;
    }
    public String getYear() {
        return yearType;
    }
    public void setYear(String year) {
        this.yearType = year;
    }
    public String getImdbLink() {
        return imdbLink;
    }
    public void setImdbLink(String imdbLink) {
        this.imdbLink = imdbLink;
    }
    public ArrayList<String> getFilmLocations() {
        return filmLocations;
    }
    public void setFilmLocations(ArrayList<String> filmLocations) {
        this.filmLocations = filmLocations;
    }

    //For setting and getting individual elements
    public void addFilmLocation(String loc){
        filmLocations.add(loc);
    }
    public String getFilmLocation(int i){
        return filmLocations.get(i);
    }

    // ------------------------------------------------------------------------
    // toString
    // ------------------------------------------------------------------------
    public String toString(){
        return name + " " + yearType;
    }
}
