package com.devflow.tsunami;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
public class FilmDataAdapter extends ArrayAdapter<FilmData> {

    private Context context;
    private ArrayList<FilmData> filmData;

    public final String         DOCUMENTARY         = "Documentary";
    public final String         TV                  = "TV Series";
    public final String         SHORTFILM           = "Short Film";
    public final String         VIDEO               = "Video";

    public FilmDataAdapter(Context context, int textViewResourceId, ArrayList<FilmData> filmData)
    {
        super(context, textViewResourceId, filmData);
        this.context = context;
        this.filmData = filmData;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {

                LayoutInflater vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = vi.inflate(R.layout.row_layout, null);
            }
            FilmData film = filmData.get(position);
            if (film != null) {
                    TextView tt = (TextView) v.findViewById(R.id.toptext);
                    TextView bt = (TextView) v.findViewById(R.id.bottomtext);
                    ImageView iv= (ImageView)v.findViewById(R.id.icon);

                    if (tt != null) {
                          tt.setText("Title: "+film.getName());
                    }
                    if(bt != null){
                          bt.setText("Year: "+ film.getYear().substring(1, 5)); //substring of 1 to 5 to get the year because the first char is "("
                    }

                    setIconByTYpe(iv, film.getYear());

            }
            return v;
    }


    private void setIconByTYpe(ImageView iv, String type)
    {
        if(type.contains(this.TV)) iv.setImageResource(R.drawable.icon_tv);
        else if(type.contains(SHORTFILM)) iv.setImageResource(R.drawable.icon_film);
        else if(type.contains(VIDEO)) iv.setImageResource(R.drawable.icon_film);
        else if(type.contains(DOCUMENTARY)) iv.setImageResource(R.drawable.icon_documentary);
        else iv.setImageResource(R.drawable.icon_movie);
    }

}
