package com.devflow.tsunami;

import java.util.ArrayList;

import android.graphics.drawable.Drawable;

import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.OverlayItem;

@SuppressWarnings("hiding")
public class HelloItemizedOverlay<HelloItemizedOverlay> extends ItemizedOverlay<OverlayItem> {

    private ArrayList<OverlayItem> mOverlays = new ArrayList<OverlayItem>();


    public HelloItemizedOverlay(Drawable defaultMarker, LocationMapActivity locationMapActivity) {
        super(boundCenterBottom(defaultMarker));
      }


    @Override
    protected OverlayItem createItem(int i)
    {
        return mOverlays.get(i);
    }

    @Override
    public int size()
    {
        return mOverlays.size();

    }

    public void addOverlay(OverlayItem overlay) {
        mOverlays.add(overlay);
        populate();
    }
}
