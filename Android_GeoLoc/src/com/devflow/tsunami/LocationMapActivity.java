package com.devflow.tsunami;

import java.util.List;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import com.devflow.parsing.AsyncMapParsing;
import com.devflow.util.Util;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;

public class LocationMapActivity extends MapActivity {

    // ------------------------------------------------------------------------
    // Fields
    // ------------------------------------------------------------------------
    private FilmData      filmData  = null;
    private MapController myMapController;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        // Initial theme set
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_layout);
        MapView mapView = (MapView) findViewById(R.id.mapview);
        mapView.setBuiltInZoomControls(true);
        mapView.preLoad();

        myMapController = mapView.getController();
        myMapController.setZoom(5); //Fixed Zoom Level

        //Get filmData
        Intent intent = getIntent();
        filmData = (FilmData) intent.getSerializableExtra("FILMDATA");

        List<Overlay> mapOverlays = mapView.getOverlays();
        Drawable drawable = this.getResources().getDrawable(R.drawable.pin_mark);
        HelloItemizedOverlay<OverlayItem> itemizedoverlay = new HelloItemizedOverlay<OverlayItem>(drawable, this);

        //Start async task
        new AsyncMapParsing(this, mapOverlays, itemizedoverlay, myMapController, filmData).execute(filmData.getImdbLink());
    }

    @Override
    protected boolean isRouteDisplayed()
    {
        // TODO Auto-generated method stub
        return false;
    }


    /*
     * Override this so the home button is used to end the activity
     */
    /*
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId()) {
        case android.R.id.home:
            finish();
        }
        return true;
    }
    */

}
