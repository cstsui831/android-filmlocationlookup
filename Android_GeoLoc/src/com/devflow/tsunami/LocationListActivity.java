package com.devflow.tsunami;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.devflow.parsing.AsyncLocationParsing;

public class LocationListActivity extends SherlockActivity {

    // ------------------------------------------------------------------------
    // Fields
    // ------------------------------------------------------------------------
    private ActionBar actionBar = null;
    private FilmData  filmData  = null;

    // ------------------------------------------------------------------------
    // onCreate, onPause, onResume
    // ------------------------------------------------------------------------
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        // Initial theme set
        setTheme(com.actionbarsherlock.R.style.Theme_Sherlock_Light);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.location_list);
        ListView resultListView = (ListView) findViewById(R.id.locationList);
        resultListView.setEmptyView(findViewById(R.id.empty));

        Intent intent = getIntent();
        filmData = (FilmData) intent.getSerializableExtra("FILMDATA");

        actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(InitialListActivity.actionBarBG);
        actionBar.setIcon(R.drawable.icon_back);
        actionBar.setTitle(filmData.getName());
        actionBar.setSubtitle("Filming Locations");

        new AsyncLocationParsing(this, resultListView, filmData).execute(filmData.getImdbLink());
    }


    /*
     * Override this so the home button is used to end the activity
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
       switch (item.getItemId())
       {
          case android.R.id.home:
              finish();
       }
    return true;
    }
}
