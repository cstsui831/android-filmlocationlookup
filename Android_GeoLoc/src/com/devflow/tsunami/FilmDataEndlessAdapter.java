package com.devflow.tsunami;

import java.util.ArrayList;

import android.content.Context;
import android.widget.ListAdapter;

import com.commonsware.cwac.endless.EndlessAdapter;

/**
 * Extends the EndlessAdapter library class to load data
 * automatically when user scrolls to the bottom of the list
 * by wrapping around the FilmDataAdapter class
 * @author Tommy
 *
 */
public class FilmDataEndlessAdapter extends EndlessAdapter {

    @SuppressWarnings("unused")
    private Context             context;
    @SuppressWarnings("unused")
    private int                 layoutResource;
    private FilmDataAdapter     originalAdapter;
    private ArrayList<FilmData> additionalfilmList;

    public FilmDataEndlessAdapter(ListAdapter wrapped)
    {
        super(wrapped);
    }

    public FilmDataEndlessAdapter(Context context, ListAdapter wrapped, int pendingResource)
    {
        super(context, wrapped, pendingResource);
        this.context = context;
        this.originalAdapter = (FilmDataAdapter) wrapped;
        this.layoutResource = pendingResource;
    }

    @Override
    protected boolean cacheInBackground() throws Exception
    {
        if (InitialListActivity.resultParser != null
                && InitialListActivity.resultParser.isFirstListDone()) {
            boolean hasNext = InitialListActivity.resultParser.hasNext();
            if (hasNext) {
                additionalfilmList = InitialListActivity.resultParser
                        .parseHTML(InitialListActivity.resultParser.getNextPageLink());
            }

            // After doing the work
            hasNext = InitialListActivity.resultParser.hasNext();
            if (hasNext)
                return true;
            else
                return false;
        }
        return true;

    }

    @Override
    protected void appendCachedData()
    {
        if (!originalAdapter.isEmpty() && additionalfilmList != null) {
            for (FilmData film : additionalfilmList) {
                originalAdapter.add(film);
            }
            originalAdapter.notifyDataSetChanged();
        }

    }

}
