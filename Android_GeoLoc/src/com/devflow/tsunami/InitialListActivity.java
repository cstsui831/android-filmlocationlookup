package com.devflow.tsunami;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import net.londatiga.android.ActionItem;
import net.londatiga.android.QuickAction;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.devflow.parsing.AsyncResultParsing;
import com.devflow.parsing.ResultParser;
import com.devflow.util.Util;

/**
 * ShowLocationActivity.java - main activity class for getting and displaying
 * the information
 **/
public class InitialListActivity extends SherlockActivity implements LocationListener {

    // ------------------------------------------------------------------------
    // Fields
    // ------------------------------------------------------------------------
    public static Drawable      actionBarBG         = null;

    private ActionBar           actionBar           = null;
    private QuickAction         quickAction         = null;
    private ListView            resultListView      = null;

    private Context             context             = null;
    private String              provider            = null;
    private String              searchURL           = null;
    public static ResultParser  resultParser        = null;
    private LocationManager     locationManager     = null;
    private FilmDataAdapter     adapter             = null;
    private ArrayList<FilmData> currentFilmList     = new ArrayList<FilmData>();

    private int                 selectedItemPos     = -1;
    private String              currentAddress      = null;

    private String              imdbURL             = "http://www.imdb.com/search/text?realm=title&field=locations&q=";
    private boolean             update              = true;
    //private String[]           stringArray         = {"Hellow"};
    // ------------------------------------------------------------------------
    // onCreate, onPause, onResume
    // ------------------------------------------------------------------------
    @SuppressWarnings("static-access")
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        // Initial theme set
        //setTheme(com.actionbarsherlock.R.style.Theme_Sherlock_Light);
        setTheme(com.actionbarsherlock.R.style.Theme_Sherlock_Light);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main);

        // Various setup method calls
        setupActionBar();
        setupListView();
        setupLocationManager();
        setupQuickAction();

        // Create the result parser that we will be using to parse html
        this.resultParser = new ResultParser();
        this.context = this;
        locationManager.requestLocationUpdates(provider, 400, 1, this);
    }


    /* Remove the locationlistener updates when Activity is paused */
    @Override
    protected void onPause()
    {
        super.onPause();
        locationManager.removeUpdates(this);
    }


    /* Request updates at startup */
    @Override
    protected void onResume()
    {
        super.onResume();
        locationManager.requestLocationUpdates(provider, 400, 1, this);
    }


    private void setupActionBar()
    {
        //actionBarBG = this.getResources().getDrawable(R.drawable.bg_blue);

        actionBarBG = this.getResources().getDrawable(R.drawable.bg_light_blue);
        actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(actionBarBG);
        //actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        //TextView txView = new TextView(this); txView.setText("Current Location");
        //SpinnerAdapter mSpinnerAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_selectable_list_item, stringArray);
        //actionBar.setListNavigationCallbacks(mSpinnerAdapter, null);
        actionBar.setTitle("FilmLocationLookup");
    }


    private void setupListView()
    {
        // GUI Setup
        this.resultListView = (ListView) findViewById(R.id.resultlist);
        resultListView.setEmptyView(findViewById(R.id.emptyView));
        adapter = new FilmDataAdapter(this, android.R.layout.list_content, currentFilmList);
        resultListView.setAdapter(adapter);
        resultListView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                quickAction.show(view);
                selectedItemPos = position;
            }
        });
    }


    private void setupLocationManager()
    {
        // Get the location manager
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        // Define the criteria how to select the location provider -> use
        // default
        Criteria criteria = new Criteria();
        provider = locationManager.getBestProvider(criteria, false);
        // Location location = locationManager.getLastKnownLocation(provider);

        // After initializing, wait for the first update
        Toast.makeText(this, "Please wait while getting location information", Toast.LENGTH_LONG)
                .show();
    }


    private void setupQuickAction()
    {
        //use setSticky(true) to disable QuickAction dialog being dismissed after an item is clicked
        ActionItem listAction = new ActionItem();
        listAction.setActionId(1);
        listAction.setTitle("List");
        listAction.setIcon(getResources().getDrawable(com.devflow.tsunami.R.drawable.icon_list));

        //Accept action item
        ActionItem mapAction = new ActionItem();
        mapAction.setActionId(2);
        mapAction.setTitle("Map");
        mapAction.setIcon(getResources().getDrawable(com.devflow.tsunami.R.drawable.icon_pin));

       //Upload action item
        ActionItem upAction = new ActionItem();
        upAction.setActionId(3);
        upAction.setTitle("Link");
        upAction.setIcon(getResources().getDrawable(com.devflow.tsunami.R.drawable.icon_link));

        quickAction = new QuickAction(this);
        quickAction.addActionItem(listAction);
        quickAction.addActionItem(mapAction);
        quickAction.addActionItem(upAction);


        //setup the action item click listener
        quickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
            @Override
            public void onItemClick(QuickAction quickAction, int pos, int actionId) {
                //ActionItem actionItem = quickAction.getActionItem(pos);
                FilmData data = adapter.getItem(selectedItemPos);
                if (actionId == 1) {
                    //Start activity for displaying a list of all other filming locations
                    Intent intent = new Intent(InitialListActivity.this, LocationListActivity.class);
                    intent.putExtra("FILMDATA", data);
                    startActivity(intent);
                }
                // Start activity for displaying a map view with plotted filming locations
                else if(actionId == 2){
                    Intent intent = new Intent(InitialListActivity.this, LocationMapActivity.class);
                    intent.putExtra("FILMDATA", data);
                    startActivity(intent);
                }
                else if(actionId == 3){
                    // Start activity for displaying the IMDb link
                    String url = data.getImdbLink();
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(browserIntent);
                }
            }
        });

        quickAction.setOnDismissListener(new QuickAction.OnDismissListener() {
            @Override
            public void onDismiss() {
            }
        });
    }


    /*THis is the onCreate for the action bar*/
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Created the "Refresh" Button
        MenuItem refreshAction = menu.add("Refresh");
        refreshAction.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        refreshAction.setIcon(com.devflow.tsunami.R.drawable.icon_refresh);

        MenuItem settingsAction = menu.add("Settings");
        settingsAction.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        settingsAction.setIcon(com.devflow.tsunami.R.drawable.icon_setting);
        // refreshAction.setIcon(android.R.drawable.ic_menu_share);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {

        if (item.getTitle().equals("Refresh")) {
            update = true;
            locationManager.requestLocationUpdates(provider, 400, 1, this);
        }

        if(item.getTitle().equals("Settings")){

        }
        return true;
    }

    // ------------------------------------------------------------------------
    // onLocationChanged, onStatusChanged, onProviderEnabled/Disabled
    // ------------------------------------------------------------------------
    /**
     * Takes care of updating the visible fields and get the current address
     */
    @Override
    public void onLocationChanged(Location location)
    {
        if(update){
            // Get GPS Coordinates
            double lat = (double) (location.getLatitude());
            double lng = (double) (location.getLongitude());

            // Set static lat lng for easy access
            Util.lat = lat;
            Util.lng = lng;

            String currentURL = getAndSetAddressTextField(lat, lng);

            //TODO NEED TO RESET VARIABLES HERE SO FILMDATAENDLESSADAPTER WON"T BE STOPPED
            // If not null and it doesn't equal the last search string
            if (currentURL != null && !currentURL.equalsIgnoreCase(searchURL)) {
                this.searchURL = currentURL;
                Log.d(Util.Tag, "URL: " + this.searchURL);
                // using locationURL here because of testing: should make it
                // searchURL for regular use
                new AsyncResultParsing(context, resultParser, currentFilmList, resultListView, adapter).execute(searchURL);
                //After this point, user should use the refresh button to manually get location
            }
            else{
                update = false;
            }
        }
    }


    @Override
    public void onStatusChanged(String provider, int status, Bundle extras)
    {
    }

    @Override
    public void onProviderEnabled(String provider)
    {
        Toast.makeText(this, "Enabled new provider " + provider, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProviderDisabled(String provider)
    {
        Toast.makeText(this, "Disabled provider " + provider, Toast.LENGTH_SHORT).show();
    }

    // ------------------------------------------------------------------------
    // Methods
    // ------------------------------------------------------------------------
    /**
     * Set the argument information to the list view
     */
    public void displayToResultList(ArrayList<String> stringList)
    {
        if (stringList != null && !stringList.isEmpty()) {
            ArrayAdapter<String> listAdapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_list_item_1, stringList);
            resultListView.setAdapter(listAdapter);
            listAdapter.notifyDataSetChanged();
        }
    }

    /**
     * Get the address from gps loc and setting it to the labels Also returns
     * the URL address
     */
    private String getAndSetAddressTextField(double lat, double lng)
    {
        Geocoder gcd = new Geocoder(this, Locale.getDefault());
        List<Address> addresses = null;

        try {
            addresses = gcd.getFromLocation(lat, lng, 1);
        }
        catch (IOException e) {
            //Toast.makeText(this, "Still trying to get address", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

        // if getting address was successful, set text field
        String searchURL = null;
        if (addresses != null && addresses.size() > 0) {
            Address bestAddress = addresses.get(0);
            String concatAddress = makeAddress(bestAddress.getAddressLine(0),
                    bestAddress.getLocality(), bestAddress.getAdminArea(),
                    bestAddress.getCountryName());
            Log.d(Util.Tag, concatAddress);
            currentAddress = concatAddress;
            // addressText.setText(concatAddress);
            if(!concatAddress.equalsIgnoreCase(currentAddress)){
                Toast.makeText(this, currentAddress, Toast.LENGTH_LONG).show();
            }
            // Last two argument specifies the "depth" of the address to do the
            // search
            searchURL = makeSearchString(bestAddress.getAddressLine(0), bestAddress.getLocality(),
                    bestAddress.getAdminArea(), bestAddress.getCountryName(), Util.CITY,
                    Util.FILMLOCATION);
        }
        return searchURL;
    }


    /**
     * Concatenate together the address string for printing out on screen
     */
    private String makeAddress(String addressLine, String city, String state, String country)
    {
        String completeAddress = addressLine + ", " + city + ", " + state + ", " + country;
        // String completeAddress = addressLine + "\n" + city + "\n" + state +
        // "\n" + country;
        return completeAddress;
    }


    /**
     * Concatenate the URL string to be scraped using Jsoup
     */
    private String makeSearchString(String addressLine, String city, String state, String country,
            String depth, String searchType)
    {


        String completeAddress = "";

        if (searchType.equals("FILMLOCATION")) {
            completeAddress = city + "+" + state;
        }
        else {
            completeAddress = addressLine + "+" + city + "+" + state + "+" + country;
        }
        completeAddress = completeAddress.replaceAll(" ", "+");
        String URL = imdbURL + completeAddress;
        return URL;
    }
}